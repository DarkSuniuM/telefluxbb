"""Main bot file."""

import datetime as dt
import time
import logging

import jdatetime as jdt
import pytz
from requests.exceptions import ConnectionError

from src.telefluxbb.bot.base import bot
from src.telefluxbb.config import Config
from src.telefluxbb.feed_reader import read_feed
from src.telefluxbb.utils import (read_latest_post_id, read_template,
                                  write_latest_post_id)

MESSAGE_TEMPLATE = read_template()

jdt.set_locale('fa_IR')


while True:
    logging.info('Getting new data.')
    latest_post_id = read_latest_post_id()
    logging.info(f'Latest post ID: {latest_post_id}')
    try:
        posts = read_feed(Config.FEED_URL)
    except ConnectionError as e:
        logging.error('Connection error!')
        logging.error(f'{repr(e)}')
        logging.info('Retrying in 10 seconds.')
        time.sleep(10)
        continue
    logging.info(f'Read {len(posts)} posts from feed.')
    for post in posts[::-1]:
        if post['id'] <= latest_post_id:
            continue
        _pub_date = dt.datetime.strptime(post['Pubdate'],
                                         '%a, %d %b %Y %H:%M:%S %z')
        _pub_date = _pub_date.astimezone(pytz.timezone('Asia/Tehran'))
        _pub_date = jdt.datetime.fromgregorian(datetime=_pub_date)
        message = MESSAGE_TEMPLATE.safe_substitute({
            'author': post['Author'],
            'title': post['Title'],
            'pub_date': _pub_date.strftime('%a, %d %b %Y %H:%M:%S'),
            'link': post['Link'],
        })
        bot.send_message(chat_id=Config.GROUP_ID, text=message)
        write_latest_post_id(post['id'])
        logging.info(
            f'Posted new message ({post["id"]}), Writing latest post id...')
    logging.info('Sleeping for 180 seconds.')
    time.sleep(180)
