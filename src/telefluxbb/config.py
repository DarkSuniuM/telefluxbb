"""Configuration file."""

import os

from dotenv import load_dotenv

load_dotenv()


class Config:
    """Basic Config Class."""

    BOT_TOKEN = os.getenv('BOT_TOKEN')
    BOT_PROXY = os.getenv('BOT_PROXY')
    GROUP_ID = os.getenv('GROUP_ID')
    FEED_URL = os.getenv('FEED_URL')
