"""Feed reader utility functions."""

import re
from xml.etree import ElementTree as ET

import requests


def get_data(url: str) -> ET.Element:
    """Get XML Data from URL."""
    req = requests.get(url=url)
    return ET.fromstring(req.text)


def get_posts(root_element: ET.Element):
    """Extract posts from root element."""
    return filter(lambda element: element.tag == 'item', root_element[0])


def serialize_post(post_element: ET.Element) -> dict:
    """Serialize post from XML Element to Dictianory."""
    output = {
        element.tag.title(): element.text for element in post_element
    }
    output['id'] = int(output['Link'].split('?id=')[-1])
    output['Author'] = re.findall(r'^.+\s\((.+)\)$', output['Author'])[0]
    return output


def read_feed(url: str) -> list:
    """Read and serialize posts from feed URL."""
    data = get_data(url=url)
    posts = get_posts(root_element=data)
    return [serialize_post(post_element=post) for post in posts]
