"""Bot module's base file."""

from telegram.bot import Bot
from telegram.utils.request import Request

from src.telefluxbb.config import Config


def _proxy():
    config_proxy_value = Config.BOT_PROXY.lower().strip()
    disable_proxy_values = ['0', 'false', '']
    if config_proxy_value in disable_proxy_values:
        return None
    return config_proxy_value


def _request():
    return Request(proxy_url=_proxy())


bot = Bot(token=Config.BOT_TOKEN, request=_request())
