"""TeleFluxBB Utilities."""


import os
import string


def get_project_directory_path():
    """Get project's main directory path."""
    return os.path.normpath(os.path.join(os.path.dirname(__file__), '../../'))


def read_latest_post_id():
    """Read latest notified post ID from file-system."""
    file_path = os.path.join(get_project_directory_path(), '.latest_post')
    try:
        with open(file_path, 'rb') as file:
            return int(file.read().decode())
    except (FileNotFoundError, ValueError):
        with open(file_path, 'wb') as file:
            file.write(b'0')
    return 0


def write_latest_post_id(post_id: int):
    """Write latest notified post ID to file-system."""
    if not isinstance(post_id, int):
        raise TypeError('post_id has to be an integer.')
    file_path = os.path.join(get_project_directory_path(), '.latest_post')
    with open(file_path, 'wb') as file:
        file.write(f'{post_id}'.encode())


def read_template():
    """Read notification message template."""
    file_path = os.path.join(get_project_directory_path(), 'message.template')
    try:
        with open(file_path, 'rb') as file:
            file_content = file.read().decode('UTF-8')
            return string.Template(file_content)
    except FileNotFoundError:
        raise FileNotFoundError(f'Can\'t open file "{file_path}". Seems like the file is not found!')
